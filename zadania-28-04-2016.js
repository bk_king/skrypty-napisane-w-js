//1.Utwórz „przestrzeń nazw” MYAPP. Będziemy do niej dodawać kolejne funkcje.
MyApp = {};

//2.Dodaj metodę display(), która dla podanej liczby sekund wyświetli na konsoli podany tekst.
MyApp.display = function (seconds, tekst){
    function displayOut(){
        console.log(tekst);
    }
    setTimeout(displayOut, seconds*1000);
}
//MyApp.display(1, "Wyświetl text");

//3.Dodaj metodę random(), która co podaną liczbę sekund wyświetli na konsoli tablicę 10 losowych liczb z podanego przedziału.
MyApp.random = function(seconds, from, to){
    function randomOut(){
        var randomValue = null, arrayOfResult=[];
        for(i=0; i<10; i++){
        randomValue = Math.round(Math.random()*(to-from)+from);
        arrayOfResult.push(randomValue);
        }
        console.log(arrayOfResult);
    }
    setInterval(randomOut, seconds*1000);
}
//console.log(MyApp.random(1, 1,200));

//4.Dodaj metodę randomAnom(), która będzie działała tak jak metoda random(), jednak użyj w niej funkcji anonimowej jako funkcji zwrotnej dla funkcji setInterval().
MyApp.randomAnom = function (seconds, from, to){
    setInterval(function(){
        var randomValue = null, arrayOfResult=[];
        for(i=0; i<10; i++){
        randomValue = Math.round(Math.random()*(to-from)+from);
        arrayOfResult.push(randomValue);
        }
        console.log(arrayOfResult);
    }, seconds*1000);
}
//console.log(MyApp.randomAnom(1, 20, 30));

//5.Dodaj metodę primaAprilis(), która w zadanym przedziale zwróci tablicę lat, w których 1 kwietnia przypada we wtorek.
MyApp.primaAprilis = function(from, to){
    var arrayOfYears =[];
    for(i=from; i<=to; i++){
    console.log(date = new Date(i, 3, 1));
    console.log(date.getDay());
    if(date.getDay()===2) arrayOfYears.push(i);
    }
    return arrayOfYears;
}
//console.log(MyApp.primaAprilis(2000, 2012));

//6.Dodaj metodę, która wylosuje liczbę od 1 do 5, poprosi użytkownika o zgadnięcie liczby, a następnie poinformuje, czy użytkownik odgadł liczbę.
MyApp.superGame = function(){
    var test = prompt("Odgadnij liczbę od 1 do 5 =)");
    var password = Math.round(Math.random()*4+1), i=1;
    while(test!=password){
        test = prompt("Nie zgdałeś, zgaduj dalej =)");
        i++;
        console.log("próba: "+i);
    }
    return "udalo Ci się za: "+i+" próbą, gratuluję";
}
//console.log(MyApp.superGame());

//7.Dodaj metodę factorial(), która rekurencyjnie zwróci silnię podanej liczby.
MyApp.fuctorial = function(n){
    if(n===0) return 1;
    return MyApp.fuctorial(n-1)*n;
}
console.log(MyApp.fuctorial(10));

//8.Dodaj metodę timeConvert(), która zamieni liczbę minut na liczbę godzin i minut (200 => 3:20).
MyApp.timeConvert = function(minutes){
    var hours = Math.floor(minutes/60);
    var mins = minutes%60;
    return hours+":"+mins;
}
//console.log(MyApp.timeConvert(200));

//9.Bubble sort
MyApp.bubbleSort = function(array){
    var lengthOfArray = array.length-1, current = 0, counter=1;
    for(var i = 0; i<=lengthOfArray; i++){
        for(var j = lengthOfArray; j>0; j--){
            if(array[j]<array[j-1]){
                current = array[j-1];
                array[j-1] = array[j];
                array[j] = current;
            }
            console.log("fukncja wykonala sie: "+counter++);
        }
    }
    return array;
}
console.log(MyApp.bubbleSort([9,8,7,6,5,4,3,2,1,20,0]));

//10.Dodaj metodę sum(), która przyjmie dowolną liczbę argumentów i zwróci ich sumę.
MyApp.sum = function(){
    var result = 0;
    var length = arguments.length;
    for(var i=0; i<length; i++){
        result+=arguments[i];
    }
    return result;
}
//console.log(MyApp.sum(1,2,3,4,5,6));

//11.Dodaj metodę fibonacci(n), która rekurencyjnie zwróci tablicę pierwszych n liczb ciągu Fibonacciego. 
MyApp.fibonacci = function(n){
    var previous = [];
    if (n===2) return [1,1];
    previous = MyApp.fibonacci(n-1);
    previous.push(previous[previous.length-1]+previous[previous.length-2]);
    return previous;
}
console.log(MyApp.fibonacci(10));

//12.Dodaj metodę average(), która przyjmie dowolną liczbę argumentów i zwróci ich średnią.
MyApp.average = function(){
    var sum = 0, length = arguments.length;
    for(i=0; i<length; i++){
        sum+=arguments[i];
    }
    return sum/length;
}
//console.log(MyApp.average(5,5,5,1));

//DODATKOWO rekurencja potęgi
MyApp.power = function(n, p){
    if(p===0) return 1;
    return MyApp.power(n,p-1)*n;
}
console.log(MyApp.power(3,3));
//13.Dodaj metodę currentDate(), która wypisze na konsolę aktualną datę.
MyApp.currentDate = function(){
    var date = new Date();
    return date;
}
//console.log(MyApp.currentDate());

//14.Dodaj metodę niceCurrentDate(), która wypisze na konsolę aktualną datę sformatowaną następująco: „28 kwietnia 2016 r.”
MyApp.niceCurrentDate = function(){
    var date = new Date();
    var arrayOfMonths = ["stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca","sierpnia", "września", "października", "listopada", "grudnia"];
    return date.getDate()+" "+arrayOfMonths[date.getMonth()]+" "+date.getFullYear()+"r.";
}
//console.log(MyApp.niceCurrentDate());

/*15.Zaimplementuj kalkulator działalności gospodarczej. Dodaj metodę fiskus(), która poprosi
użytkownika kolejno o: 
-wysokość miesięcznych przychodów, 
-wysokość miesięcznych kosztów, 
-wysokość składki ZUS na ubezpieczenie zdrowotne, 
-wysokość składki ZUS na ubezpieczenie społeczne, 
-stawkę procentową podatku dochodowego
-kwotę zmniejszającą podatek,
a następnie obliczy, ile prowadzący działalność gospodarczą zarobił
w ciągu roku „na rękę”.*/

MyApp.fiskus = function(){
    var przychody = parseFloat(prompt("Podaj wysokość miesięcznych przychodów"));
    var koszty = parseFloat(prompt("Podaj wysokość miesięcznych kosztów"));
    var spoleczne = parseFloat(prompt("Podaj składkę na ubezpieczenie społeczne"));
    var zdrowotne = parseFloat(prompt("Podaj składkę na ubezpieczenie zdrowotne"));
    var podatek = parseFloat(prompt("Podaj podatek dochodowy"));
    var ulga = parseFloat(prompt("podaj kwote ulgi podatkowej"));
    if (isNaN(przychody && koszty && spoleczne && zdrowotne && ulga)) return false;
    
    var profits=przychody-koszty-spoleczne;
    var podatekDochodowy = profits*podatek-zdrowotne*0.861;
    if(podatekDochodowy<0) podatekDochodowy = 0;
    console.log(podatekDochodowy);
     
    return 12*(profits - podatekDochodowy - zdrowotne)-ulga;
}
//console.log(MyApp.fiskus());

