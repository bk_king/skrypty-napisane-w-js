//1. zadanie dla chętnych - funkcja, która skróci pierwszy człon adresu (przed @) o połowę.
function protectEmail(email){
    var newEmail = email.split("@");
    var HalfLenghtOfWord = Math.round(newEmail[0].length/2);
    var HalfWord =  newEmail[0].substr(0, HalfLenghtOfWord);
    newEmail[0] = HalfWord+"...@";
    return newEmail.join('');
}
console.log(protectEmail("1111@wp.pl"));

//2. Zadanie dla chętnych - Sito Eratostenesa
function fastPrimes(from, to){
    //jeśli argument mniejszy od 2 funkcja zwraca false
    if(from<1 || to<2) return false;
    //deklaracja zmiennych
    var zmienna = 1,arrayOfResult=[], counter=1;
    //Tworzy się tablica do wartości "to" podanej w argumencie funkcji
    for (var k=2; k<=to; k++){
        arrayOfResult[k]=k;
    }
    for(var i=2; i<=to; i=zmienna){
    //jeżeli w tablicy arrayOfResult nie ma wartości zmienna to do zmiennej dodajemy 1 i sprawdzamy ponownie
     do{
        zmienna++;
     }while(arrayOfResult.indexOf(zmienna)===-1 && zmienna<=to);
    
     for(var j = zmienna; j<=to; j+=zmienna){
         //jeżeli w tablicy jest wartość zmienna to usuwamy wszystkie jej wielokrotności
        if(arrayOfResult.indexOf(j+zmienna)!==-1) delete arrayOfResult[arrayOfResult.indexOf(j+zmienna)];   
     }
    }
    //zwracamy przyciętą tablicę do podanego przedziału
    return arrayOfResult=arrayOfResult.slice(from,to+1);
}
console.log(fastPrimes(1,200));

//3. Sprawdza poprawność wpisanego nr PESEL
function isCorrectPesel(pesel){
    var flag = true;
    if(isNaN(pesel)){
      flag = false;
    } 
    if(pesel.length<11 || pesel.length>11) flag = false;
    if (flag){
      pesel = String(pesel);
      var arrayOfFactors = [1,3,7,9,1,3,7,9,1,3], result = 0, modulo = null;
        for(i=0; i<10; i++){
        result += parseInt(pesel.charAt(i))*arrayOfFactors[i];
        }
        modulo = result%10;
        flag = 10-modulo===parseInt(pesel.charAt(10)) ? "to jest poprawny pesel" : "to jest błędny pesel";
    }
    return flag;
}

console.log(isCorrectPesel("90090310179"));
