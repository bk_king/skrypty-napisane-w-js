//1.Sprawdzy czy podany rok jest przestepny

/*function isLeapYear(rok){
    if(rok%4===0){
        if(rok%400===0){
            return "To jest rok przestępny";
        }else if(rok%100!==0){
            return "to jest rok przestępny";
        }
    }return "to nie jest rok przestępny";
}

console.log(isLeapYear(1960));*/

//2.Sprawdza czy podany podany rok jest rokiem przestępnym za pomocą operatorów || && i skróconej notacji
/*function isLeapYear2(rok){
    var leapYear = (rok%4===0 && rok%100!==0) || (rok%400===0) ? "to jest rok przestępny" :  "to nie jest rok przestępny";
    return leapYear;
}
console.log(isLeapYear2(2012));*/

//3.Zwraca losowy element podanej tablicy

/*function randomItem(array){
    var randomElement = Math.round(Math.random()*(array.length-1));
    console.log(array);
    return array[randomElement];
}
console.log("Wylosowana wartość z tablicy to: "+randomItem([1,4,6,3,2,7,3]));*/

//4. Funkcja, która oblicza pole koła dla podanej średnicy
/*function circleArea(diameter){
    var radius = diameter/2;
    var areaCircle = (Math.PI*Math.pow(radius, 2)).toFixed(2);
    return parseFloat(areaCircle);
    
}
console.log(circleArea(6));*/

//5. Funckja zwraca kwotę wynagrodzenia po odjęciu 18%
/*function withoutTax(amount){
    return Math.round(amount - amount*0.18);
}
console.log("Twoja kwota po odjęciu podatku to 18% to: "+withoutTax(2000));*/
//6.funckja, która skraca podany ciąg znaków do podanej liczby
/*function truncate(string, amount){
    return "Słowo "+string+" zostało skrócone do "+amount+" znaków, czyli: "+string.slice(0, amount);
}
console.log(truncate("ivanowiski", 3));*/
//7. Sprawdza czy a zawiera się w b
/*function includes(a, b){
    var exist = b.indexOf(a)!==-1 ? "wyraz "+a+" zawiera się w ciągu "+b : "wyraz "+a+" nie zawiera się w ciągu "+b;
    return exist;
}
console.log(includes("ma", "ala ma kota"));*/
//8. funckja, która zwróci pierwszy element tablicy, zawierający podane słowo lub false, jeśli słowo nie zostanie znalezione.
/*function search(word, array){
    for(var key in array){
         if(array[key]===word){
             var exist = "znaleziono słowo "+word+" w indesie nr: "+key; 
             break;
         }
    }
    return exist = exist ? exist : false;
}
console.log(search("cos",["ala", "kot","pies","ma","cos","ma"]));*/
//9.Napisz funkcję randomString(), która zwróci losowy łańcuch znaków o podanej długości.
/*function randomString(length){
    var i, counter="",
        array = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","r","s","t","u","w","y","z"],
        count = array.length; 
    for(i=1; i<=length; i++){
        counter+=array[Math.round(Math.random()*(count-1))];
    }
    return "Losowy ciąg składający się z "+length+" znaków to: "+counter;
}
console.log(randomString(12));*/
//10.funkcja która jako parametr przyjmie łańcuch znaków (wyrazy rozdzielone spacją) i zwróci najdłuższy wyraz.
/*function  getLongestWord(sentence){
    var array = sentence.split(" "), count=array.length, longest=0, theLongestWorld="";
    for(i=0; i<count; i++){
        if(array[i].length>longest){
            longest = array[i].length;
            theLongestWorld = array[i];
        } 
    }
    return "Nadluzsze slowo w podanym ciagu to: "+theLongestWorld;
}
console.log(getLongestWord("dlugie slowo ciekawe, ktory wyraz najdluzszy"));*/
//11.funkcja stringToArray(), która skonwertuje łańcuch znaków na listę słów.
/*function stringToArray(string){
    var array = string.split(" ");
    return array;
}
console.log(stringToArray("dlugi ciag znakow"));*/
//12.funkcja isPalindrome(), która sprawdzi, czy podane słowo jest palindromem.
/*function isPalindrome(string){
    var palindronme = string.split('').reverse().join('');
    palindronme = palindronme === string ? "To słowo jest palindromem" : "to slowo nie jest palindromem";
    return palindronme;
}
console.log(isPalindrome("zarazs"));*/
//13. funckcja truncate(), która skróci zdanie znaków to podanej liczby słów.
/*function truncate(sentence, count){
   sentence = sentence.substr(0, count);
    return sentence;
}
console.log(truncate("to zdanie jest dlugie", 9));*/
//14.funkcja reverseNumber(), która odwróci podaną liczbę (1234 => 4321).
/*function reverseNumber(number){
    number = String(number);
    return parseFloat(number.split('').reverse().join(''));
}
console.log(reverseNumber(5678));*/
//15.funkcja alphabetOrder(), która zwróci podany łańcuch znaków z literami według alfabetu („wyraz” - „arwyz”).
/*function alphabetOrder(string){
    string = string.split('').sort(function(a,b){ return a>b;}).join('');
    return string;
}
console.log(alphabetOrder("wyraz"));*/
//16.funkcja firstUppercase(), która przyjmie zdanie i zamieni pierwszą literę każdego słowa na wielką.
/*function firstUppercase(sentence){
    sentence = sentence.split(' ');
        var length = sentence.length, arrayWord = [], firstLetter="", rest="";
    for(var i=0; i<length; i++){
        firstLetter = sentence[i].charAt(0).toUpperCase();
        rest = sentence[i].substr(1, sentence[i].length-1);
        arrayWord[i] = firstLetter+rest;
    }
    return arrayWord.join(' ');
}
console.log(firstUppercase("to jest zdanie, które zamieni literę w każdym wyrazie na dużą"));*/
//17. funkcja factors(), która zwróci tablicę wszystkich dzielników podanej liczby.
/*function factors(liczba){
    var arrayofFactors = [], j=0;
    for(var i=1; i<=liczba; i++){
        if( liczba%i===0 ){
            arrayofFactors[j]=i;
            j++
        }
    }
    return "Wsyzstkie dzielniki liczby "+liczba+" to: "+arrayofFactors;
}
console.log(factors(100));*/
//18. funkcja primes(from, to), która zwróci tablicę wszystkich liczb pierwszych z podanego przedziału.
/*function isPrime(liczba){
    var i=null;
    if(liczba<=1) return false;
    for(i=2; i<liczba; i++){
        if(liczba%i===0) {
        return false;
        }
    }
    return true;
}
console.log(isPrime(3));
function primes(from, to){
    var arratOfPrime = [], j=0;;
    for(var i = from; i<to; i++){
        if (isPrime(i)){
            arratOfPrime[j] = i;
            j++;
        }
    }
    return arratOfPrime;
}
console.log(primes(-9,9));*/
//19. Napisz funkcję vowelCount(), która zwróci liczbę samogłosek w podanym łańcuchu znaków.
/*function vowelCount(string){
    var vowelArray = ["a","ą", "e","ę", "i","o","u", "y"];
    var length = string.length;
    var result=0;
    var lengthVowelArray = vowelArray.length;
    for (var i = 0; i<length; i++){
        for(var j = 0; j<lengthVowelArray; j++){
            if(string[i]===vowelArray[j]){
                result++;
            }
        }
    }
    return "liczba samogłosek w wyrazie "+string+" to: "+result;
}
console.log(vowelCount("karabin"));*/

