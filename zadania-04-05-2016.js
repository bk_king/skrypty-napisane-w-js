var car1 = {
    brand: "audi",
    model: "A6",
    power: 125

};
var car2 = {
    brand: "BMW",
    model: "7",
    power: 193

};
var car3 = {
    brand: "Mazda",
    model: "6",
    power: 136

};
var car4 = {
    brand: "McLaren",
    model: "F1",
    power: 627

};
//zadanie 1. stwórz obiekt circle, który będzie przechowywał długość swojej średnicy i będzie potrafił podać swoje pole i obwód
var circle ={diameter: 12,
             circuit: function(){
                 return (Math.PI * this.diameter).toFixed(2);
             },
             area: function(){
                 var r = this.diameter/2;
                 return  (Math.PI * r * r).toFixed(2);
             }
            }
//console.log("pole koła to: "+circle.area());
//console.log("obwód koła to: "+circle.circuit());

//zadanie 2. napisz funkcję maxPower(), która przyjmie dowolną liczbę samochodów i zwróci markę i model tego o największejmocy silnika
function maxPower(){
    var i, length = arguments.length, car, max=0, model;
    for(i=0; i<length; i++){
        car = arguments[i];
        if(arguments[i].power>max){
            max=arguments[i].power;
            model = "model: "+arguments[i].model+", marka "+arguments[i].brand;
        }
    }
    return model;
}
//console.log(maxPower(car1, car2, car3, car4));

//zadanie 3. napisz funkcję averagePower(), która przyjmie tablicę samochodów i zwróci ich średnią moc
function averagePower(array){
    var i, car, sum=0, length=array.length;
    for(var i = 0; i<length; i++){
        sum += array[i].power;
    }
    return sum/length;
}
//console.log(averagePower([car1, car2, car3, car4]));

//zadanie 4. stwórz obiekt oraz dwie funkcje, które sprawdzą poprawność PESELU oraz DOWODU OSOBISTEGO i dodają nowe pole do obiektu
var person = {
    name: "Bartosz",
    surname: "Król",
    setPesel: function(peselNo){
        if (!isCorrectPesel(peselNo)) {
                console.error("Nieprawidłowy numer PESEL");
                return false;
        }
        this.pesel = peselNo;
    },
    setID: function(id){
        if(!isCorrectId(id)){
            console.error("Nieprawidłowy numer dowodu");
            return false;
        }
        this.idNumber = id;
    }
}
//console.log(person);
//console.log(person.setPesel("90090410179"));
//console.log(person.setID("AFB557908"));
//console.log(person);

//zadanie 5. napisz funkcę females(), która zwróci tablice imion kobiet z podanej tablicy osób
persons = [
    {name: 'Nicole', pesel: '33121718040'},
    {name: 'Marcin', pesel: '65092518811'},
    {name: 'Krzysztof', pesel: '84101003692'},
    {name: 'Anna', pesel: '03240215485'}
];
function females(array){
    var i, length = array.length, arrayOfFemales=[];
    for(i=0; i<length; i++){
        if(array[i].pesel.charAt(9)%2===0){
            arrayOfFemales.push(array[i].name);
        }
    }
    return arrayOfFemales;
}
//console.log(females(persons));

//zadanie 6. Napisz funkcję carSort, która z uzyciem funkcji sort posortuje tablicę obiektów wg mocy silnika od najw do najm i odwrotnie
function carSort(array){
    array.sort(function(a,b){
        if(a.power>b.power) return -1;
        else if(a.power<b.power) return 1;
        return 0;
    });
    return array;
}
//console.log(carSort([car1,car2,car3,car4]));

//zadanie 7. Napisz funkcję carSort, która z uzyciem funkcji quickSort posortuje tablicę obiektów wg mocy silnika
function quickSort(array){
    var pivot = array[0], arrayLeft=[], arrayRight=[];
    console.log("nowe przejscie" +array);
    console.log("pivot: "+pivot);
    if(array.length<2){
        return array;
    }
    for(var i=1; i<array.length; i++){
        if(array[i].power>pivot.power){
            
            arrayLeft.push(array[i]);
            console.log("left"+arrayLeft);
        }else{
            arrayRight.push(array[i]);
            console.log("right"+arrayRight);
        }
    }
     return quickSort(arrayLeft).concat(pivot, quickSort(arrayRight));
}
console.log(quickSort([car1,car2,car3,car4]));

//FUNKCJE POMOCNICZE Z POPRZEDNICH ZADAŃ
function isCorrectId(id){
    var letters="A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z",
    idArray = id.split(''),
    letters = letters.split('  '), 
    length = idArray.length,
    arrayOfRatio = [7, 3, 1, 9, 7, 3, 1, 7, 3],
    sum=0;
    if(length<9){
        console.error("Nie poprawny numer ID - Sprawdź ilość znaków");
        return false;
    }
    for(var i=0; i<3; i++){
        idArray[i] = (letters.indexOf(idArray[i])+10);
    }
    for (var j = 0; j<length; j++) {
    sum+=idArray[j]*arrayOfRatio[j];
    }
    return sum%10?false:true;
}

function isCorrectPesel(pesel){
    var flag = true;
    if(isNaN(pesel)){
      flag = false;
    } 
    if(pesel.length<11 || pesel.length>11) flag = false;
    if (flag){
      pesel = String(pesel);
      var arrayOfFactors = [1,3,7,9,1,3,7,9,1,3], result = 0, modulo = null;
        for(i=0; i<10; i++){
        result += parseInt(pesel.charAt(i))*arrayOfFactors[i];
        }
        modulo = result%10;
        flag = 10-modulo===parseInt(pesel.charAt(10)) ? true : false;
    }
    return flag;
}