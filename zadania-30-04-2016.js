//1. Napisz funkcję fibonacci(n), nie używając rekurencji, która zwróci n-ty wyraz Ciągu Fibonacciego
function fibonacci(n){
    var current = 1, result = 1, g=2;
    if(n<2) return 1;
    while(g<n){
        i=result;
        result = i + current;
        console.log(i+" + "+current);
        current = i; 
        g++;
    }
    return result;
}
//console.log(fibonacci(3));

//2. Napisz funkcję abs(), która przyjmie dowolną liczbę argumentów i zwróci ten o największej wartości bezwzględnej
function abs(){
    var length = arguments.length, currentMax=0, absoluteValue=null;
    for(i=0; i<length; i++){
        absoluteValue = Math.abs(arguments[i]);
        if(currentMax<absoluteValue) currentMax=absoluteValue;
    }
    return currentMax;
}
//console.log(abs(1,-101,5,-100,6,-102));

//3. Napisz funkcję triangle(), która w zależnoci od liczby argumentów obliczy pole trójkąta na podstawie długości podstawy i wysokości lub na podstawie długości wszystkich jego boków.  
function triangle(a,b,c){
    var halfOfRound = 0, heron=0;
    if (arguments.length<2){
        console.error("Za mało argumentów do obliczenia pola trójkąta");
        return false;
    }else if(arguments.length===2){
        return a*10/2;
    }
    halfOfRound = (a+b+c)/2;
    heron =  Heron = halfOfRound * (halfOfRound - a) * (halfOfRound - b) * (halfOfRound - c);
    if (heron <0 ){
        console.error("podane długości boków nie utworzą trójkata");
        return false;
    }
    return Math.sqrt(heron);
}
//console.log(triangle(5,5,5));

//4. Napisz funkcję square(), która na podstawie podanych współczynników równania kwadratowego, zwróci tablicę wszystkich jego rozwiązań. 
function square(a,b,c){
    var delta = b*b-4*a*c, x1, x2;
    if(delta<0){
        return [];
    }else if(delta===0){
        return [-b/(2*a)];
    }
    x1 = (-b-Math.sqrt(delta))/(2*a);
    x2 = (-b+Math.sqrt(delta))/(2*a);
    return [x1, x2];
}
//console.log(square(1, 2, 1));

//5. Napisz funkcję calculator(), która obliczy proste działanie matematyczne takie jak np. 3 + 6 . Funkcja ma obsługiwać dodawanie, odejmowanie, mnożenie oraz dzielenie. 
function calculator(string){
    if(string.includes("+")){
        string=string.split("+");
        return parseFloat(string[0])+parseFloat(string[1]);
    }else if(string.includes("-")){
        string=string.split("-");
        return (string[0])-(string[1]);
    }else if(string.includes("*")){
        string=string.split("*");
        return (string[0])*(string[1]);
    }else if(string.includes("/")){
        string=string.split("/"); 
        return (string[0])/(string[1]);
    }
}
//console.log(calculator("3/3"));

//6. Napisz funkcję multiples(a,b), która zwróci tablicę wszystkich wielokrotności mniejszych od b. 
function multiples(a,b){
    array=[];
    for(i=a; i<=b; i+=a){
    array.push(i);
    }
    return array;
}
//console.log(multiples(3,60));

//7a. Napisz funkcję commonFactor(a, b), która iteracyjnie zwróci największy wspólny dzielnik dwóch liczb na podstawie niezoptymalizowanego Algorytmu Euklidesa. (while)
function commonFactorA(from,to){
    var factor = 1, array=[], current;
    if(to<from){
            current = to;
            to = from;
            from = current;
    }
    while(factor!==0){
       factor = to - from;
       to=from;
       from=factor;
        if(to<from){
            current = to;
            to = from;
            from = current;
        }
    }
    return to;
}
//console.log(commonFactorA(28,24));

//7b. (for)
function commonFactorB(from, to){
    var factor = 1, array=[], current;
    if(to<from){
            current = to;
            to = from;
            from = current;
    }
    for(var i=0; factor>0; i++){
       factor = to - from;
       to=from;
       from=factor;
        if(to<from){
            current = to;
            to = from;
            from = current;
        }
    }
    return to;
    
}
//console.log(commonFactorB(28,24));

//7c. (modulo)
function commonFactorC(from, to){
    var factor = 1, array=[], current;
    if(to<from){
            current = to;
            to = from;
            from = current;
    }
    for(var i=0; factor>0; i++){
       factor = to % from;
       to=from;
       from=factor;
        if(to<from){
            current = to;
            to = from;
            from = current;
        }
    }
    return to;
}
//console.log(commonFactorB(18,12));

//8. Napisz funkcję recCommonFactor(a, b), która to samo zadanie rozwiąże w sposób rekurencyjny.
function recCommonFactor (from, to){
    var current;
    if(to===0||from===0) return from+to;
    if(to<from){
            current = to;
            to = from;
            from = current;
        }
    return recCommonFactor(from, to%from);
}
//console.log(recCommonFactor(18,12));

//9. Napisz funkcję geoMean(), która zwróci średnią geometryczną podanej tablicy
function geoMean(array){
    var length = array.length, average = 1;
    for(i=0; i<length; i++){
       average *= array[i];
    }
    return Math.pow(average, 1/length);
}
//console.log(geoMean([1,2,4]));

//10. Napisz funkcję isCorrectId(), która sprawdzi, czy podany numer dowodu osobistego jest prawidłowy.
function isCorrectId(id){
    var letters="A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z",
    idArray = id.split(''),
    letters = letters.split('  '), 
    length = idArray.length,
    arrayOfRatio = [7, 3, 1, 9, 7, 3, 1, 7, 3],
    sum=0;
    if(length<9){
        console.error("Nie poprawny numer ID - Sprawdź ilość znaków");
        return false;
    }
    for(var i=0; i<3; i++){
        idArray[i] = (letters.indexOf(idArray[i])+10);
    }
    for (var j = 0; j<length; j++) {
    sum+=idArray[j]*arrayOfRatio[j];
    console.log(sum);
    }
    return sum%10?false:true;
}
//console.log(isCorrectId("ASD016763"));

//11. Napisz funkcję modulo(from, to, n), która zwróci tablicę wszystkich liczb podzielnych przez n z podanego przedziału.
function modulo(from, to, n){
    var array=[];
    for(var i=from; i<=to; i++){
        if(!(i%n)){
            array.push(i);
        }
    }
    return array;
}
//console.log(modulo(10, 20, 3));

//12. Napisz funkcję truncate(sentence, chars), która skróci podane zdanie do podanej liczby znaków. Jeśli na końcu miałby pozostać fragment  kolejnego wyrazu, niech funkcja usunie cały wyraz. Jeśli podano większą liczbę znaków niż długość łańcucha, niech funkcja na końcu doda wielokropek. 
function truncate(sentence, chars){
    var length = sentence.length, array=[], result="";
    console.log(length);
    if(length<=chars){
        return sentence;
    }
    sentence = sentence.split(' ');
    for(i=0; result.length+sentence[i].length+1<=chars; i++){
       result+=" "+sentence[i];
    }
    return result + "…";
}
//console.log(truncate("To jest szkolenie i w ogole", 3));
//13. Napisz własną implementację algorytmu quick sort.
function quickSort(array){
    var pivot = array[0], arrayLeft=[], arrayRight=[];
    console.log("nowe przejscie" +array);
    console.log("pivot: "+pivot);
    if(array.length<2){
        return array;
    }
    for(var i=1; i<array.length; i++){
        if(array[i]<pivot){
            
            arrayLeft.push(array[i]);
            console.log("left"+arrayLeft);
        }else{
            arrayRight.push(array[i]);
            console.log("right"+arrayRight);
        }
    }
     return quickSort(arrayLeft).concat(pivot, quickSort(arrayRight));
}
//console.log(quickSort([1,8,3,5,2,7]));

//13b. Napisz własną implementację algorytmu quick sort używając pivota jako srodka wartość trzech elementów.
function middleElement(a,b,c){
    if(a>b){
        if(a>c){
            return b>c ? b:c;
        }else return a;
    }else if(b>c){
         return a>c ? a:c;
    }else return b;
}
console.log("Srodkowy element to: "+middleElement(12,10,8));

function quickSortB(array){
    var pivot = middleElement(array[0], array[Math.floor(array.length/2)], array[array.length-1]), arrayLeft=[], arrayRight=[];
    console.log("nowe przejscie" +array);
    console.log("pivot: "+pivot);
    if(array.length<2){
        return array;
    }
    for(var i=0; i<array.length; i++){
        if(array[i]===pivot){
            continue;
        }
        if(array[i]<pivot){
            arrayLeft.push(array[i]);
            console.log("left"+arrayLeft);
        }else{
            arrayRight.push(array[i]);
            console.log("right"+arrayRight);
        }
    }
     return quickSortB(arrayLeft).concat(pivot, quickSortB(arrayRight));
}
//console.log(quickSortB([1,8,3,5,2,7]));

//14. Napisz funkcję indexOf(array, search), która w sposób optymalny pod względem złożoności obliczeniowej zwróci indeks szukanego elementu w posortowanej tablicy nazwisk lub -1, jeśli element nie występuje.  
function indexOf(array, search){
    var middleIndex = Math.floor(array.length/2), leftIndex=0, rightIndex=array.length-1;
    while(leftIndex<=rightIndex){
        if(array[middleIndex]===search){
            return middleIndex;
        }if(array[middleIndex]<search){
            leftIndex = middleIndex + 1;
        }else{
           rightIndex = middleIndex - 1;
        }
         middleIndex = Math.floor((rightIndex + leftIndex) / 2);
    }
    return -1;
}
//console.log(indexOf(["Agnieszka", "Beata", "Krzysztof", "Tomek", "Wanda"], "Wanda"));
